﻿#include <stdio.h>

int main()
{
	int n1, n2, n3, n4;
	//scanf("%d %d %d %d\n", n4, n3, n2, n1);
	n1 = 1;
	n2 = 0;
	n3 = 0;
	n4 = 127;
	
	if(!((0<=n1<=255)&&(0<=n2<=255)&&(0<=n3<=255)&&(0<=n4<=255)))
	    printf("Incorrect IP Address.");
	else
	    printf("Human readable IP address is: %d.%d.%d.%d IP address as a 32-bit number: %lu\n", n4, n3, n2, n1, n1 + n2*256 + n3*256*256 +n4*256*256*256);
	
	
	return 0;
} 