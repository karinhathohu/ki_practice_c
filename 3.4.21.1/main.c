﻿#include <stdio.h>

int main()
{
    int howMany;
    int i;
    scanf("%d", &howMany);

	for(i = 0; (i < howMany)&&(i < 20); i++){
	    printf("*");
	    for(int j = 0; j < i;j++){
	        printf(" ");
	    }
	    printf("*\n");
	}
	if (howMany >= 20)
	    i = 20;
	else 
	    i = howMany;
	for(i; (i >= 0);i--){
	    printf("*");
	    for(int j = 0; j < i;j++){
	        printf(" ");
	    }
	    printf("*\n");
	}
	return 0;
}