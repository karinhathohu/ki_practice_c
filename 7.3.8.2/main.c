#include <stdio.h>

// space is  33
//	printf("%c", 97); - a
// 'a' - 97
// arr[i]
// printf("%c", i + 97)
int main(){
	FILE *f;
	char *fnameOriginal;
	char current;
	int arr[26] = {0};
	int total = 0;
	int spaces = 0;
	int lines = 0;
	
	fnameOriginal = "metricme.txt";
	f = fopen(fnameOriginal, "r");
	if (f == NULL){
		printf("Error opening a file.");
		return 0;
	}	
	current = fgetc(f);
	lines++;
    while(current != EOF){
		total++;
		if (current == ' ')
			spaces++;
		else if (((int)current >= 97)&&((int)current <= 97+26)){
			arr[(int)current - 97]++;
		}			
		else if (current == '\n'){
			lines++;
		}
		current = fgetc(f);
    } 
	fclose(f);
	
	printf("Lines: %d\n", lines);
	printf("Whitespaces: %d\n", spaces);
	printf("Characters: %d\n", total);
	for(int i = 0; i < 26; i++){
		printf("Small letter: %c : %d\n", (char)(i+97), arr[i]);
	}
	return 0;
}