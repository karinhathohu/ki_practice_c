﻿#include <stdio.h>

int main()
{
	int i;
	scanf("%d", i);
	if((i>255)||(i<0)){
	    return 0;
	}
	
	printf("H nibble: %d L nibble: %d", i >> 4, i & 0x0F);
	
	return 0;
}