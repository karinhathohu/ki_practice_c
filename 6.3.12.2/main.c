	#include <stdio.h>
	int getValue(int paramA, float paramB){
		int result = 0;
		if (paramA>10)
		{
			result += 2;
		}
		else
		{
			result += 1;
		}
		if (paramB>5.5)
		{
			result += 4;
		}
		else
		{
			result += 3;
		}
		return result;
	}
	int getOneOrTwo(int param){
		if(param > 5)
			return 2;
		return 1;
	}
	int main(void)
	{
		int fiveValue = getValue(6, 6); /* your code */
		int sixValue = getValue(11, 11); /* your code */
		int sevenValue = getOneOrTwo(6) + getValue(6,6); /* your code */
		int eightValue = getValue(11, 7) + getOneOrTwo(8); /* your code */
		int nineValue = getValue(7, 7)*getOneOrTwo(7) - getOneOrTwo(1); /* your code */
		printf("Five: %d\n", fiveValue);
		printf("Six: %d\n", sixValue);
		printf("Seven: %d\n", sevenValue);
		printf("Eight: %d\n", eightValue);
		printf("Nine: %d\n", nineValue);
		return 0;
	}
