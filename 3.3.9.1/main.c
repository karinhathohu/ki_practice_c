﻿#include <stdio.h>

int main()
{
	float myNumber = 2.9;
	int myINTNumber = (int)myNumber;
	
	if((myINTNumber >= 1)&&(myINTNumber < 2)){
	    printf("Very bad.");   
	}
	else if((myINTNumber >= 2)&&(myINTNumber < 3)){
	    printf("Bad."); 
	}
	else if((myINTNumber >= 3)&&(myINTNumber < 4)){
	    printf("Neutral."); 
	}	
	else if((myINTNumber >= 4)&&(myINTNumber < 5)){
	    printf("Good."); 
	}
	else if((myINTNumber >= 5)&&(myINTNumber < 6)){
	    printf("Very good."); 
	}
	return 0;
}