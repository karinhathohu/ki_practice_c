﻿#include <stdio.h>
#include <iostream>

int main()
{
    int day, month, year;
    std::cin >> day;
    std::cin >> month;
    std::cin >> year;
    
    bool leap;
	/* because you may not know the else instruction yet, 
	   this simple formula will help you to check if a year is a leap year */
		if (year % 400 == 0)
			leap = true;
		else if (year % 100 == 0)
			leap = false;
		else if (year % 4 == 0)
			leap = true;
	
    switch (month) {
        case 12:
            day += 30;
        case 11:
            day += 31;
        case 10: 
            day += 30;
        case 9:
            day += 31;
        case 8:
            day += 31;
        case 7:
            day += 30;
        case 6:
            day += 31;
        case 5:
            day += 30;
        case 4:
            day += 31;
        case 3:
            if (leap)
                day += 29;
            else 
                day += 28;
        case 2:
            day += 31;
    }
	
	printf("The day of the year: %d", day);
	
	return 0;
}