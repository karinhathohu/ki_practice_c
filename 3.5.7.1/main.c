﻿#include <stdio.h>

int main()
{
	int currentYear;
	scanf("%d", &currentYear);
	
    if(currentYear % 4 == 0){
        if(currentYear % 100 == 0){
            if(currentYear % 400 == 0){
                printf("%d is a leap year.", currentYear);
            }
            else {
            printf("%d is not a leap year.", currentYear);
            }
        }
        else{
            printf("%d is a leap year.", currentYear);
        }
    }
    else{
        printf("%d is not a leap year.", currentYear);
    }
    
	return 0;
}