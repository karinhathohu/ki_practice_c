﻿#include <stdio.h>
#include <iostream>

int main()
{
    int day, month;
    std::cin >> day;
    std::cin >> month;
    
    int countdays = 0;
    switch (month) {
        case 12:
            countdays += 30;
        case 11:
            countdays += 31;
        case 10: 
            countdays += 30;
        case 9:
            countdays += 31;
        case 8:
            countdays += 31;
        case 7:
            countdays += 30;
        case 6:
            countdays += 31;
        case 5:
            countdays += 30;
        case 4:
            countdays += 31;
        case 3:
            countdays += 29;
        case 2:
            countdays += 31;
    }
    countdays += day;
    
    printf("The day of the year: %d", countdays);
	return 0;
}