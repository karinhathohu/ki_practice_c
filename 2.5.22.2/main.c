﻿#include <stdio.h>
#include <iostream>

int main()
{
    float f1, f2;
	std::cin >> f1;
	std::cin >> f2;
	
	printf("Value A: %f Value B: %f %.6f + %.6f = %.6f. %.6f - %.6f = %.6f. %.6f * %.6f = %.6f.", f1, f2, f1, f2, f1 + f2, f1, f2, f1 - f2, f1, f2, f1*f2);
	
	return 0;
}