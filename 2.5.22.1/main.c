﻿#include <stdio.h>
#include <iostream>

int main()
{
    int w;
    float pi;
	
	std::cin >> w;
	std::cin >> pi;
	
	printf("How many days in the week: %d\n", w);
	printf("The value of Pi to two points: %.2f\n", pi);
	printf("There are %d in a week.\n", w);
	printf("Pi value is %.6f.\n", pi);
	
	return 0;
}