﻿#include <stdio.h>

int main()
{
    int arr[10];
    arr[0] = 1;
    arr[1] = 1;
    printf("%d\n%d\n", arr[0], arr[1]);
    for(int i = 2;i<10;i++){
        arr[i] = arr[i-1] + arr[i-2];
        printf("%d\n",arr[i]);
    }
    for(int i = 0;i<10;i+=2){
        printf("%d\n",arr[i]);
    }
    for(int i = 1;i<10;i+=2){
        printf("%d\n",arr[i]);
    }
	return 0;
}