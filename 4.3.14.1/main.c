﻿#include <stdio.h>

int main()
{
	float numbers[10] = {5.6, 4.3, 6.2, 6.4, 7.3, 2.3, 8.3, 9.2, 0.1, 1.9};
	bool done = false;
	float temp;
	while (!done){
	    done = true;
	    for (int i = 0; i < 10 - 1; i++){
	        if(numbers[i] < numbers[i+1]){
	           temp = numbers[i];
	           numbers[i] = numbers[i+1];
	           numbers[i+1] = temp;
	           done = false;
	        }
	    }
	    for(int i = 0; i < 10; i++){
	        printf("%.2f ", numbers[i]);
	    }
	    printf("\n", numbers[9]);
	}
    for(int i = 0; i < 10; i++){
	    printf("%.2f ", numbers[i]);
	}
	printf("\n", numbers[9]);
	return 0;
}